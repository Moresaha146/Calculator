<?php
    class Calculator{
        private $value1;
        private $value2;
        public function setValue($input1='', $input2=''){
            $this->value1 = $input1;
            $this->value2 = $input2;
        }
        public function getValue(){
            return $this->value1+$this->value2;
        }
    }

    $cal = new Calculator();
    $cal->setValue($_POST['val1'],$_POST['val2']);
    echo $cal->getValue();